//
//  UIAlertController+Ext.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/9/10.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func message(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        let root = UIApplication.shared.keyWindow?.rootViewController
        root?.present(alert, animated: true, completion: nil)
    }
    
    static func input(_ title: String, isSecure: Bool = false, callback: @escaping (String) -> Void) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: {field in
            field.isSecureTextEntry = isSecure
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            guard let text = alert.textFields?.first?.text, !text.isEmpty else {
                UIAlertController.input(title, callback: callback)
                return
            }
            
            callback(text)
        })
        
        let root = UIApplication.shared.keyWindow?.rootViewController
        root?.present(alert, animated: true, completion: nil)
    }
}
