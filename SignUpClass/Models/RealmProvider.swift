//
//  RealmProvider.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/9/5.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

struct RealmProvider {
    let configuration: Realm.Configuration
    
    internal init(config: Realm.Configuration) {
        configuration = config
    }
    
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    // MARK: - School realm
    private static let schoolConfig = Realm.Configuration(
        fileURL: try! Path.inLibrary("school.realm"),
        schemaVersion: 1,
        objectTypes: [Student.self, Course.self, Teacher.self, Location.self])
    
    public static var school: RealmProvider = {
        return RealmProvider(config: schoolConfig)
    }()
}
