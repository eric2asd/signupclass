//
//  School.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/9/5.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

class SchoolModel {
    
    private let provider: RealmProvider
    init(provider: RealmProvider = .school) {
        self.provider = provider
    }
    
    func courses() -> Results<Course>{
        return provider.realm.objects(Course.self)
    }
    
    func dayOfCourses(date: Date) -> Results<Course>{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        

        let predicate = NSPredicate(format: "%K == %@",Course.Properties.dateStr.rawValue,formatter.string(from: date))
        return self.courses().filter(predicate)
    }
    
    func student(id: Int) -> Student? {
        let student = provider.realm.object(ofType: Student.self, forPrimaryKey: id)
        return student
    }
    
    func realm() -> Realm {
        return provider.realm
    }
    
    func students() -> Results<Student> {
        return provider.realm.objects(Student.self)
    }
    
}
