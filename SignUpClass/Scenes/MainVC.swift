//
//  MainVC.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/9/10.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    let school = SchoolModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StudentVC {
            guard let student = sender as? Student else { return }
            vc.student = student
        }
    }
    
    @IBAction func pushToStudentVC(_ sender: Any) {
        UIAlertController.input("StudentID") { [weak self] id in
            guard let id = Int(id), let student = self?.school.student(id: id) else { return }
            self?.performSegue(withIdentifier: "ShowStudentVC", sender: student)
        }
        
    }
    
    
    
    
    
    

}
