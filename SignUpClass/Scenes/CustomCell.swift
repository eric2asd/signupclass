//
//  CustomCell.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/8/30.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell: JTAppleCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!

}
