//
//  StudentVC.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/9/8.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import UIKit

class StudentVC: UIViewController {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var usedCoursesLabel: UILabel!
    @IBOutlet weak var totalCoursesLabel: UILabel!
    var student: Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        guard let student = student else {
            return
        }
        idLabel.text = "\(student.id)"
        nameLabel.text = "\(student.name)"
        levelLabel.text = "\(student.level)"
        usedCoursesLabel.text = "\(student.usedCourses)"
        totalCoursesLabel.text = "\(student.totalCourses)"
    }

}
