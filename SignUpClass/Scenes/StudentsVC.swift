//
//  StudentsVC.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/10/5.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import UIKit
import RealmSwift

class StudentsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let school = SchoolModel()
    var students: Results<Student>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        students = school.students()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StudentVC {
            guard let student = sender as? Student else { return }
            vc.student = student
        }
    }

}

extension StudentsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = students?[indexPath.row].name
        
        return cell
    }
}

extension StudentsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let student = students?[indexPath.row] else { return }
        self.performSegue(withIdentifier: "ShowStudentVC", sender: student)
    }
}
