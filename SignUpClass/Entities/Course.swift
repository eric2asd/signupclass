//
//  Course.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/8/31.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Course: Object {
    enum Properties: String {
        case key
        case date
        case dateStr
        case timeStr
        case location
        case level
        case teacher
        case openPeople
        case totalPeople
        case maxPeople
    }
    
    dynamic var key = UUID().uuidString
    dynamic var date = Date.distantPast
    dynamic var dateStr = ""
    dynamic var timeStr = ""
    dynamic var location: Location?
    dynamic var level = 0
    dynamic var teacher: Teacher?
    dynamic var openPeople = 4
    dynamic var totalPeople = 6
    dynamic var maxPeople = 9
    
    let students = LinkingObjects(fromType: Student.self,
                                        property: "courses")

    convenience init(date: Date, location: Location, level: Int, teacher: Teacher) {
        self.init()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        self.date = date
        self.dateStr = formatter.string(from: date)
        formatter.dateFormat = "hh:mm"
        self.timeStr = formatter.string(from: date)
        self.location = location
        self.level = level
        self.teacher = teacher
    }
    
    public override static func primaryKey() -> String? {
        return Properties.key.rawValue
    }

}

extension Course {
    @discardableResult
    func add(to realm: Realm) -> Course {
        try! realm.write {
            realm.add(self, update: true)
        }
        return self
    }
}
