//
//  Location.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/8/31.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Location: Object {
    enum Properties: String {
        case name
        case address
    }
    dynamic var name = ""
    dynamic var address = ""
    
    let courses = LinkingObjects(fromType: Course.self,
                                  property: "location")
    
    convenience init(name: String, address: String) {
        self.init()
        self.name = name
        self.address = address
    }
    
    public override static func primaryKey() -> String? {
        return Properties.name.rawValue
    }
}

extension Location {
    @discardableResult
    func add(to realm: Realm) -> Location {
        try! realm.write {
            realm.add(self, update: true)
        }
        return self
    }
}
