//
//  Student.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/8/31.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Student: Object {
    enum Properties: String {
        case id
        case name
        case level
        case totalCourses
        case usedCourses
    }
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var level = 0
    dynamic var totalCourses = 0
    dynamic var usedCourses = 0
    
    let courses = List<Course>()
    
    convenience init(id: Int, name: String, level: Int, totalCourses: Int, usedCourses: Int = 0) {
        self.init()
        self.id = id
        self.name = name
        self.level = level
        self.totalCourses = totalCourses
        self.usedCourses = usedCourses
    }
    
    public override static func primaryKey() -> String? {
        return Properties.id.rawValue
    }
}

extension Student {
    @discardableResult
    func add(to realm: Realm) -> Student {
        try! realm.write {
            realm.add(self, update: true)
        }
        return self
    }
}
