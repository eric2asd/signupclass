//
//  Teacher.swift
//  SignUpClass
//
//  Created by 陳信毅 on 2018/8/31.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Teacher: Object {
    enum Properties: String {
        case key
        case name
    }
    
    dynamic var key = UUID().uuidString
    dynamic var name = ""
    
    let courses = LinkingObjects(fromType: Course.self,
                                 property: "teacher")

    convenience init(name: String) {
        self.init()
        self.name = name
    }
    
    public override static func primaryKey() -> String? {
        return Properties.key.rawValue
    }

}

extension Teacher {
    @discardableResult
    func add(to realm: Realm) -> Teacher {
        try! realm.write {
            realm.add(self, update: true)
        }
        return self
    }
}
